import React, { ReactNode } from "react";
interface Props {
  children: ReactNode;
  onClose: () => void;
}
function Alert({ children, onClose }: Props) {
  return (
    <div className="alert alert-primary alert-dismissble">
      {children}
      <button
        type="button"
        className="btn-close"
        onClick={onClose}
        data-bs-dismiss=""
      ></button>
    </div>
  );
}

export default Alert;
