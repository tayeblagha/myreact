import React, { useEffect, useState } from "react";
import { Button, Input, Table } from "antd";

import { Container, Paper } from "@mui/material";

export default function Student(): JSX.Element {
  const paperStyle = { padding: "50px 20px", width: 600, margin: "20px auto" };
  const [text, setText] = useState<string>("");
  const [tasks, setTasks] = useState<any[]>([]);

  const apiUrl = import.meta.env.VITE_REACT_APP_API_URL;

  const handleAddTask = (e: React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();

    const newTask = { text };

    fetch(apiUrl + "/tasks", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(newTask),
    })
      .then(() => {
        console.log("New Task added");
        setText("");
        return fetch(apiUrl + "/tasks");
      })
      .then((res) => res.json())
      .then((result) => {
        setTasks(result);
      })
      .catch((error) => {
        console.error("Error adding task:", error);
      });
  };

  useEffect(() => {
    fetch(apiUrl + "/tasks")
      .then((res) => res.json())
      .then((result) => {
        setTasks(result);
      })
      .catch((error) => {
        console.error("Error fetching tasks:", error);
      });
  }, []);

  const handleDeleteTask = async (id: string) => {
    try {
      await fetch(`${apiUrl}/tasks/${id}`, {
        method: "DELETE",
      });

      setTasks(tasks.filter((task) => task._id !== id));
    } catch (error) {
      console.error("Error deleting task:", error);
    }
  };

  const columns = [
    {
      title: "Task",
      dataIndex: "text",
      key: "text",
    },
    {
      title: "Actions",
      key: "actions",
      render: (text: string, record: any) => (
        <Button
          className="btn btn-danger"
          onClick={() => handleDeleteTask(record._id)}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="16"
            height="16"
            fill="currentColor"
            className="bi bi-trash"
            viewBox="0 0 16 16"
          >
            <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5m2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5m3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0z" />
            <path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4zM2.5 3h11V2h-11z" />
          </svg>
        </Button>
      ),
    },
  ];

  return (
    <Container>
      <Paper elevation={3} style={paperStyle}>
        <h1 style={{ color: "blue" }}>
          <u>Add Task</u>
        </h1>
        <Input
          placeholder="Task"
          value={text}
          onChange={(event) => setText(event.target.value)}
        />
        <h1></h1>
        <center>
          <Button className="btn btn-primary" onClick={handleAddTask}>
            Add Task
          </Button>{" "}
        </center>
      </Paper>
      <h1>Tasks</h1>
      <Paper elevation={3} style={paperStyle}>
        <Table dataSource={tasks} columns={columns} />
      </Paper>
    </Container>
  );
}
