import { useState } from "react";
import Alert from "./components/Alert";
import Button from "./components/Button";
import ListGroup from "./components/ListGroup";
import { AppBar } from "@mui/material";

import Student from "./components/Student";
function App() {
  let items = ["New York", "San Francisco", "Tokyo", "London", "Paris"];
  const handleSelectItem = (item: string) => {
    console.log(item);
  };
  const [alertvisible, setAlertVisibility] = useState(false);
  return (
    <div>
      {/* <h1> hi {import.meta.env.VITE_REACT_APP_API_URL}</h1> */}
      <h1> hi </h1>

      <Student />
      {/* {alertvisible && (
        <Alert onClose={() => setAlertVisibility(false)}> my alert</Alert>
      )}
      <Button onClick={() => setAlertVisibility(!alertvisible)}>
        My button
      </Button> */}
      {/* <Alert>
        <h1>Hello</h1> word
      </Alert>
      <ListGroup
        items={items}
        heading="Cities"
        onSelectItem={handleSelectItem}
      />{" "} */}
    </div>
  );
}
export default App;
